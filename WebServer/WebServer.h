#ifndef WEBSERVER_H_
#define WEBSERVER_H_

#include "stm32f10x.h"


//#define EXIT_SUCCESS		0
//#define EXIT_FAILURE 		1



class WebServer {

private:

	void 	ethernet_init(void);
	uint8_t web_socket_init(uint8_t, uint16_t);
	void	print_webpage(uint32_t *, uint8_t);

public:

			WebServer(void);

	uint8_t	check_for_job(uint32_t *, uint8_t);
	void	ethernet_turn_on_and_init(void);
	void	ethernet_turn_off(void);


};


//void 	ethernet_init(void);
//uint8_t web_socket_init(uint8_t, uint16_t);
//uint8_t web_server_check_for_job(void);
//void	print_webpage(void);
//void	ethernet_turn_on_and_init(void);
//void	ethernet_turn_off(void);


#endif /* WEBSERVER_H_ */
