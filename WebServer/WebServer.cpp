#include "WebServer.h"
#include "bluepill.h"
#include "stm32f10x.h"
#include "spi1.h"
#include "stm32f10x_gpio.h"
#include "wizchip_conf.h"
#include "socket.h"
#include "serial2.h"
#include "delay.h"
#include "pc13led.h"
#include "Mp3Player.h"

#include <stdio.h>
#include <string.h>

extern uint32_t phone_numbers_table[];

int16_t retVal, sockStatus;
int16_t rcvLen;
//uint8_t rcvBuf[1000];
uint8_t bufSize[] = {2, 2, 2, 2, 2, 2, 2, 2};
uint8_t led_status;

wiz_NetInfo netInfo = { .mac 	= {0xf7, 0xbb, 0x0f, 0x22, 0x4b, 0x45},	// Mac address
                        .ip 	= {192, 168, 1, 13},					// IP address
                        .sn 	= {255, 255, 255, 0},					// Subnet mask
                        .gw 	= {192, 168, 1, 1},						// Gateway address
						.dns	= {192, 168, 1, 1},
						.dhcp   = (dhcp_mode) 1
};

wiz_NetInfo netInfo_check;


WebServer::WebServer(void) {

	reg_wizchip_cs_cbfunc(cs_sel, cs_desel);
	reg_wizchip_spi_cbfunc(spi_rb, spi_wb);

	spi1_init();

	ethernet_turn_off();
	delay(1000);
	ethernet_turn_on_and_init();
}


void WebServer::ethernet_init(void) {
	serial2_send("Ethernet init begin\n");

	//reg_wizchip_cs_cbfunc(cs_sel, cs_desel);
	//reg_wizchip_spi_cbfunc(spi_rb, spi_wb);


	while(1) {
		char msg[20];
		wizchip_init(bufSize, bufSize);
		wizchip_setnetinfo(&netInfo);
		wizchip_getnetinfo(&netInfo_check);

		sprintf(msg, "ip: %u.%u.%u.%u", netInfo_check.ip[0], netInfo_check.ip[1], netInfo_check.ip[2], netInfo_check.ip[3]);
		serial2_send(msg);
		if ((netInfo.ip[0] == netInfo_check.ip[0]) &&
			(netInfo.ip[1] == netInfo_check.ip[1]) &&
			(netInfo.ip[2] == netInfo_check.ip[2]) &&
			(netInfo.ip[3] == netInfo_check.ip[3])) {
				serial2_send(" - OK\n");
				break;
		}
		else {
			serial2_send(" - not OK, retrying...\n");
			//ethernet_turn_off();
			delay(1000);
			//ethernet_turn_on_and_init();
		}
	}
	serial2_send("Ethernet init end - OK.\n\n");
} //ethernet_init()


uint8_t WebServer::web_socket_init(uint8_t socket_number, uint16_t port) {
	char msg[100];
	//open socket
	sprintf(msg, "Opening socket [%u] on port [%u]: ", socket_number, port);
	serial2_send(msg);
	retVal = socket(socket_number, Sn_MR_TCP, port, 0);
	sprintf(msg, "(%i)", retVal);
	serial2_send(msg);
	if (retVal == socket_number) {
		serial2_send(" - OK.\n");
	} else {
		serial2_send(" - not OK.\n");
		return 1;
	}
	delay(100);

	//check socket status
	serial2_send("Socket status: ");
	retVal = getSn_SR(socket_number);
	sprintf(msg, "(0x%x)", retVal);
	serial2_send(msg);
	if (retVal == 0x13) { //SOCK_INIT
		serial2_send(" - (SOCK_INIT): OK.\n");
	} else {
		serial2_send(" - not OK.\n");
		return 1;
	}
	delay(100);

	//enable listen mode for web socket (2)
	if (socket_number == 2) {
		retVal = listen(2);
		serial2_send("Socket listen mode: ");
		retVal = getSn_SR(2);
		sprintf(msg, "(0x%x)", retVal);
		serial2_send(msg);
		if (retVal == 0x14) { //SOCKET_LISTEN
			serial2_send(" - OK.\n");
		} else {
			serial2_send(" - not OK.\n");
			return 1;
		}
		delay(100);
	}

	return retVal;
} //web_socket_init()


//uint8_t web_server_check_for_job(void) {
uint8_t WebServer::check_for_job(uint32_t * table, uint8_t size) {

	uint8_t rcvBuf[100 + size * 13];
	char msg[50];
	uint8_t remoteIP[4];
	uint16_t remotePort;
	int8_t retVal;
	uint8_t is_input_table_modified = 0;   //no

	//web socket [2] check
	if ((sockStatus = getSn_SR(2)) == SOCK_ESTABLISHED) {
		serial2_send("Connection established.\n");
		// Retrieving remote peer IP and port number
		getsockopt(2, SO_DESTIP, remoteIP);
		getsockopt(2, SO_DESTPORT, (uint8_t*)&remotePort);
		sprintf(msg, "request from: %u.%u.%u.%u:%u\n", remoteIP[0], remoteIP[1], remoteIP[2], remoteIP[3], remotePort);
		serial2_send(msg);
		//receive request
		serial2_send("Receiving request:\n");
		retVal = recv(2, rcvBuf, sizeof(rcvBuf));
		if (retVal < 0) {
			sprintf(msg, "error: %d\n", retVal);
			serial2_send(msg);
		} else if (retVal == 0) {
			serial2_send("no data received.\n");
		} else {
			for (uint8_t i = 0; i < retVal; i++) {
				serial2_send_char(rcvBuf[i]);
			}
			serial2_send("\n");

			//check if there is update request
			for (uint8_t i = 0; i < size; i++) {

				uint16_t index;
				uint16_t length = strlen((const char *) rcvBuf);
				uint32_t temp_val = 0;
				char msg[20];

				sprintf(msg, "n%i=", i+FIRST_NUMBER_SOUND);
				index = index_of((const char *) rcvBuf, (const char *) msg, 0);

				if (index != length) {

					serial2_send("updating \n");

					index += strlen(msg);

					temp_val = 0;

					for (uint16_t j = index; rcvBuf[j] >= '0' && rcvBuf[j] <= '9'; j++) {
						temp_val *= 10;
						temp_val += rcvBuf[j] - 0x30;
					}

					sprintf(msg, "number: %i == %lu\n", i, temp_val);
					serial2_send(msg);

					table[i] = temp_val;
					is_input_table_modified = !0;

				} //if
			} //for

			//respond with web-page
			if (strncmp("GET ", (char *) &(rcvBuf[0]), 4) == 0) {
				print_webpage(table, size);
				serial2_send("webpage sent.\n");
			} else {
				retVal = recv(2, rcvBuf, 100);
				sprintf(msg, "retVal: %i\n", retVal);
				serial2_send(msg);
				retVal = send(2, (uint8_t *) "ok", strlen("ok"));
			}
		} //if (retVal == 0)

		//delay(50);
		disconnect(2);
		close(2);
		serial2_send("web socket [2] closed.\n");
		delay(200);
		//web_socket_init(2, 80);
	} //if ((sockStatus = getSn_SR(2)) == SOCK_ESTABLISHED)

	// web socket [2] re-open
	retVal = getSn_SR(2);
	if ( retVal == SOCK_CLOSED )  {
		sprintf(msg, "Socket status (0x%x) - resetting\n", retVal);
		serial2_send(msg);
		ethernet_turn_off();
		delay(100);
		ethernet_turn_on_and_init();
		web_socket_init(2, 80);
	} //if

	return is_input_table_modified;
} //web_server_check_for_job()


void WebServer::print_webpage(uint32_t * table, uint8_t size) {
	uint8_t webpage[1000];

	sprintf((char *) webpage,
			"<pre>"
			"<form action=\"/\">"
			"MP3 Phone\nIP: %u.%u.%u.%u\n\n",
			netInfo.ip[0], netInfo.ip[1], netInfo.ip[2], netInfo.ip[3]);
	send(2, webpage, strlen((const char *) webpage));

	sprintf((char *) webpage,
			"\\01\\001.mp3: ready sound (beep)\n"
			"\\01\\002.mp3: wrong number sound\n"
			"\\01\\003.mp3: no answer sound\n"
			"\\01\\004.mp3: busy sound\n"
			"\\01\\005.mp3: reserved (not used)\n"
			"<form>");
	send(2, webpage, strlen((const char *) webpage));

	for (uint8_t n = 0; n < size; n++) {
		sprintf((char *) webpage,
				"\\01\\%.3u.mp3: <input type=\"number\" name=\"n%u\" min=\"0\" max=\"999999999\" value=\"%lu\">\n",
				n+FIRST_NUMBER_SOUND, n+FIRST_NUMBER_SOUND, table[n]);
		send(2, webpage, strlen((const char *) webpage));
	}

	sprintf((char *) webpage,
			"\n"
			"<input type=\"submit\" value=\"Update\">"
			"</form>"
			"</pre>");
	send(2, webpage, strlen((const char *) webpage));

} //print_webpage()


void WebServer::ethernet_turn_on_and_init(void) {
	GPIO_WriteBit(GPIOA, GPIO_Pin_3, Bit_SET);
	delay(1000);
	ethernet_init();
} //ethernet_turn_on()


void WebServer::ethernet_turn_off(void) {
	serial2_send("resetting ethernet module\n");

	GPIO_WriteBit(GPIOA, GPIO_Pin_3, Bit_RESET);
	delay(10);
} //ethernet_turn_off()
