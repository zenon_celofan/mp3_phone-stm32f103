#include "bluepill.h"
#include "serial2.h"
#include "delay.h"
#include "spi1.h"
#include "Led.h"
#include "eeprom.h"
#include "Telephone.h"
#include "Mp3Player.h"
#include "NvMemory.h"
#include "WebServer.h"
#include "socket.h"
#include "wizchip_conf.h"

#include <stdio.h>
#include <string.h>
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"


#define NUMBER_OF_PHONE_NUMBERS			20		//max 50

//states of state machine
#define STANDBY_STATE				0
#define HANDSET_PICKED_UP_STATE		1
#define DIALING_STATE				2
#define CALLING_STATE				3
#define BUSY_STATE					4
#define IDLE_STATE					5

#define IDLE_HANDSET_TIMEOUT_MS		20000
#define DIAL_TIMEOUT_MS 			5000

#define UNDEFINED			-2



int pulses;
int32_t number = 0;
char temp_str[100];
uint8_t state;
int pulse_handler = LOW;
unsigned long timer = 0;

Led board_led(GPIOC, GPIO_Pin_13, ACTIVE_LOW); //pc13led_init();

uint16_t VirtAddVarTab[NUMBER_OF_PHONE_NUMBERS * 2];
uint32_t phone_numbers_table[NUMBER_OF_PHONE_NUMBERS];
uint16_t temp_val;


void main() {

	serial2_init();			//debug serial
	serial2_send("\n\n\n************** RESET *************\n\n");

	millis_init();
	uint32_t timeout_reference;
	delay(100); //
	NvMemory	eeprom;

	Telephone	phone;

	Mp3Player	mp3_player;
	serial2_send("OK\n");
	WebServer	web_server;



	serial2_send("Reading phone numbers read from NV memory...");
	eeprom.read_phone_numbers(phone_numbers_table, NUMBER_OF_PHONE_NUMBERS);
	serial2_send("OK\n");

	delay(1000);

	serial2_send("Init finished\n\n");


	serial2_send("state == STANDBY\n");
	timeout_reference = millis();
	state  = STANDBY_STATE;


	while (1) {

		//web page handler
		if (web_server.check_for_job(phone_numbers_table, NUMBER_OF_PHONE_NUMBERS) != 0) {
			eeprom.write_phone_numbers(phone_numbers_table, NUMBER_OF_PHONE_NUMBERS);
		}

		//state machine
		if (state == STANDBY_STATE) {

			if (mp3_player.is_stopped() == false) {
				mp3_player.stop();
			}

			if (phone.is_handset_picked_up() == true) {
				mp3_player.play_in_loop(BEEP_SOUND);
				pulses = 0;
				number = UNDEFINED;
				state = HANDSET_PICKED_UP_STATE;
				timeout_reference = millis();
				serial2_send("state == HANDSET_PICKED_UP\n");
			}

		} else if (phone.is_handset_picked_up() == false) {
			serial2_send("handset put down.\n");
			state = STANDBY_STATE;
			continue;
		} //STANDBY_STATE


		if (state == HANDSET_PICKED_UP_STATE) {

			if (millis() > timeout_reference + IDLE_HANDSET_TIMEOUT_MS) {
				serial2_send("idle handset timeout\n");
				serial2_send("state == BUSY_STATE\n");
				mp3_player.play_in_loop(BUSY_STATE);
				state = BUSY_STATE;
			}

			if (phone.is_dialing() == true) {
				serial2_send("dialing\n");
				do {
					mp3_player.stop();
				} while (mp3_player.is_stopped() != true);
				state = DIALING_STATE;
				timeout_reference = millis();
				number = UNDEFINED;
				serial2_send("state == DIALING_STATE\n");
			}
		} //HANDSET_PICKED_UP_STATE


		if (state == DIALING_STATE) {

			if (millis() > timeout_reference + DIAL_TIMEOUT_MS) {
				serial2_send("dial timeout\n");
				if (number == UNDEFINED) {
					serial2_send("state == BUSY_STATE\n");
					mp3_player.play_in_loop(BUSY_STATE);
					state = BUSY_STATE;
				} else {
					serial2_send("state == CALLING_STATE\n");
					state = CALLING_STATE;
				}
			}

			if (phone.is_dialing() == true) {
				timeout_reference = millis();

				if ((GPIO_ReadInputDataBit(phone.pulse_gpio, phone.pulse_pin) == HIGH) && (pulse_handler == LOW)) {
					delay(20);
					if (GPIO_ReadInputDataBit(phone.pulse_gpio, phone.pulse_pin) == HIGH) {
						pulses++;
						pulse_handler = HIGH;
					}
				} else if (GPIO_ReadInputDataBit(phone.pulse_gpio, phone.pulse_pin) == LOW) {
					pulse_handler = LOW;
				}
			} else {
				if (pulses > 0) {

					if (number < 0) {
						number = 0;
					}

					if (pulses >= 10) {
						pulses = 0;
					}

					number = number * 10 + pulses;

					sprintf(temp_str, "                            (%i)\n", pulses);
					serial2_send(temp_str);
					pulses = 0;
					sprintf(temp_str, "--%ld--\n", number);
					serial2_send(temp_str);
				}

				if (phone.is_number_valid(number) == true) {
					serial2_send("state == CALLING_STATE\n");
					mp3_player.stop();
					state = CALLING_STATE;
				}
			} //if (phone.is_dialing() == true)

		} //DIALING_STATE


		if (state == CALLING_STATE) {

			uint8_t	to_be_played = WRONG_NUMBER_SOUND;

			if (phone.is_number_valid(number) == true) {

				for (uint8_t i = 0; i < NUMBER_OF_PHONE_NUMBERS; i++) {
					if ((uint32_t) number == phone_numbers_table[i]) {
						to_be_played = FIRST_NUMBER_SOUND + i;
						break;
					}
				}

				to_be_played = NO_ANSWER_SOUND;
			} //if (phone.is_number_valid(number) == true)

			sprintf(temp_str, "--%ld--\n", number);
			serial2_send(temp_str);
			sprintf(temp_str, "sound number: %i\n", to_be_played);
			serial2_send(temp_str);
			mp3_player.play(to_be_played);

			serial2_send("state == IDLE_STATE\n");
			state = IDLE_STATE;

		} //CALLING_STATE


		if (state == BUSY_STATE) {
			continue;
		} //BUSY_STATE


		if (state == IDLE_STATE) {
			continue;
		} //IDLE_STATE


	} //while(1)

} //main
