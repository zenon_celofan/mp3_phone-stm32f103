#include <NvMemory.h>
#include "stm32f10x.h"
#include "stm32f10x_flash.h"
#include "eeprom.h"

NvMemory::NvMemory() {

	FLASH_Unlock();
	EE_Init();

} // NvMemory()

void NvMemory::read_phone_numbers(uint32_t * table, uint8_t size) {

	uint16_t temp_val;

	for (uint8_t n = 0; n < size; n++) {
		EE_ReadVariable(EEPROM_START_ADDRESS + (n * 2), &temp_val);
		table[n] = temp_val;
		table[n] = table[n] << 16;
		EE_ReadVariable(EEPROM_START_ADDRESS + (n * 2) + 1, &temp_val);
		table[n] += temp_val;
	}

} // NvMemory::read_phone_numbers()


void NvMemory::write_phone_numbers(uint32_t * table, uint8_t size) {

	for (uint8_t n = 0; n < size; n++) {
		EE_WriteVariable(EEPROM_START_ADDRESS + (n * 2), table[n] >> 16);
		EE_WriteVariable(EEPROM_START_ADDRESS + (n * 2) + 1, table[n] & 0x0000FFFF);
	}

} // NvMemory::write_phone_numbers()
