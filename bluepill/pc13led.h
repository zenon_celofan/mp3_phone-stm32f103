/*
 * pc13led.h
 *
 *  Created on: 25 kwi 2018
 *      Author: n
 */

#ifndef PC13LED_H_
#define PC13LED_H_

void pc13led_init(void);
void pc13led_on(void);
void pc13led_off(void);
void pc13led_toggle(void);

#endif /* PC13LED_H_ */
