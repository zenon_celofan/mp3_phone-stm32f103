#ifndef LED_H_
#define LED_H_

#include "stm32f10x_gpio.h"

#define ACTIVE_LOW		Bit_RESET
#define ACTIVE_HIGH		Bit_SET

class Led {

private:
	GPIO_TypeDef * 	led_gpio;
	uint16_t		led_pin;
	BitAction		active_state;


public:
	Led(GPIO_TypeDef *, uint16_t, BitAction);

	void turn_on(void);
	void turn_off(void);
	void toggle(void);

};



#endif /* LED_H_ */
