#ifndef __SPI2_H__
#define __SPI2_H__

void 			spi2_init(void);
unsigned char	spi2_read_write(unsigned char);
unsigned char 	spi2_read_byte(void);
void 			spi2_write_byte(unsigned char writedat);

#endif /* __SPI2_H__ */
