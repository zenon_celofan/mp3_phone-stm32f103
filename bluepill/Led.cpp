#include "Led.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

Led::Led(GPIO_TypeDef * g, uint16_t p, BitAction a) {

	led_gpio = g;
	led_pin = p;
	active_state = a;

	GPIO_InitTypeDef  GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = p;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;


	if (g == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	}
	else if (g == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	}
	else if (g == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}

    GPIO_Init(g, &GPIO_InitStructure);


	turn_off();

} //Led

void Led::turn_on(void) {

	GPIO_WriteBit(led_gpio, led_pin, active_state);

} //turn_on()


void Led::turn_off(void) {

	GPIO_WriteBit(led_gpio, led_pin, (BitAction) !active_state);

} //turn_off()

void Led::toggle(void) {

	led_gpio->ODR ^= led_pin;

} //toggle()
