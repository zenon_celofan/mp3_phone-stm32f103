#ifndef __SPI1_H__
#define __SPI1_H__

#include "stm32f10x.h"

void 	spi1_init(void);
uint8_t	spi1_read_write(uint8_t);
uint8_t	spi1_read_byte(void);
void 	spi1_write_byte(uint8_t writedat);
void 	cs_sel(void);
void 	cs_desel(void);
uint8_t spi_rb(void);
void 	spi_wb(uint8_t b);

#endif /* __SPI1_H__ */
