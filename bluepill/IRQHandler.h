#ifndef IRQHANDLER_H_
#define IRQHANDLER_H_


#define RX_FRAME_SIZE					10
#define RX_FRAME_CMD_BYTE_POSITION		3
#define RX_FRAME_FDB_BYTE_POSITION		6
#define	RX_FRAME_START_BYTE				0x7E
#define	RX_FRAME_END_BYTE				0xEF


extern "C" void TIM4_IRQHandler(void);
extern "C" void EXTI15_10_IRQHandler(void);
extern "C" void USART3_IRQHandler(void);



#endif /* IRQHANDLER_H_ */
