#include "stm32f10x.h"


#ifndef NVMEMORY_H_
#define NVMEMORY_H_

class NvMemory {

public:

	NvMemory();
	void read_phone_numbers(uint32_t *, uint8_t);
	void write_phone_numbers(uint32_t *, uint8_t);

};

#endif /* NVMEMORY_H_ */
