#ifndef __DELAY_H__
#define __DELAY_H__

#include "stm32f10x.h"


void delay(uint16_t milliseconds);
void delay_us(uint16_t microseconds);

#endif /* __DELAY_H__ */
