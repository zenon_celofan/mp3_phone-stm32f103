#include "IRQHandler.h"
#include "stm32f10x_exti.h"
#include "bluepill.h"

#include "serial2.h"



extern uint32_t milliseconds_after_reset;
uint8_t rx_frame[RX_FRAME_SIZE];



void TIM4_IRQHandler(void) {

	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);

	milliseconds_after_reset++;

} //TIM4_IRQHandler


void EXTI15_10_IRQHandler(void) {

	EXTI_ClearITPendingBit(EXTI_Line10);

} //EXTI15_10_IRQHandler();


void USART3_IRQHandler(void) {

    static uint8_t byte_position_in_frame;

	uint16_t received_byte;

    if ((USART3->SR & USART_FLAG_IDLE) != 0) {
    	received_byte = (uint8_t) USART3->SR;         // Clear IDLE flag by reading status register first, and follow by reading data register
    	received_byte = (uint8_t) USART3->DR;
    	USART_ITConfig(USART3, USART_IT_IDLE, DISABLE);
    	byte_position_in_frame = 0;
    } else {
    	received_byte = (uint8_t) USART3->DR;
    	USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);
		if (received_byte == RX_FRAME_START_BYTE) {
			rx_frame[0] = received_byte;
			byte_position_in_frame = 1;
		} else if ((received_byte == RX_FRAME_END_BYTE) && (byte_position_in_frame == RX_FRAME_SIZE-1)) {
			rx_frame[RX_FRAME_SIZE-1] = received_byte;
			byte_position_in_frame = 0;
		} else {
			rx_frame[byte_position_in_frame] = received_byte;
			byte_position_in_frame++;
		}

    }
    //serial2_send(".");
} //USART3_IRQHandler()
