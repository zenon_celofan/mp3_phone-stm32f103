#include "Mp3Player.h"
#include "bluepill.h"
#include "stm32f10x_gpio.h"
#include "serial2.h"
#include "serial3.h"
#include "IRQHandler.h"
#include "delay.h"
#include <stdio.h>
#include <string.h>

#define MP3_RESPONSE_TIMEOUT	200


extern uint8_t rx_frame[RX_FRAME_SIZE];


Mp3Player::Mp3Player(void) {

	serial3_init();			//mp3 serial communication

	reset();

} //Mp3Player()


bool Mp3Player::is_stopped(void) {

#define STATUS_STOP		0x00
#define STATUS_TIMEOUT	0x03


	uint32_t response_request_time;
	uint8_t mp3_player_status;

	send_status_request();
	response_request_time = millis();

	mp3_player_status = STATUS_TIMEOUT;
	while (millis() < response_request_time + MP3_RESPONSE_TIMEOUT){

		if (rx_frame[RX_FRAME_SIZE-1] == RX_FRAME_END_BYTE) {
			if (rx_frame[RX_FRAME_FDB_BYTE_POSITION] == STATUS_STOP) {
				mp3_player_status = STATUS_STOP;
			}
			break;
		}
	}

	if (mp3_player_status == STATUS_STOP) {
		return true;
	} else {
		return false;
	}

} //is_stopped()


void Mp3Player::play(uint8_t song_number) {

	serial3_send_char(0x7E);		//header
	serial3_send_char(0xFF);		//header
	serial3_send_char(0x06);		//header
	serial3_send_char(0x03);		//CMD
	serial3_send_char(0x00);		//FEEDBACK
	serial3_send_char(0x00);		//DAT 1
	serial3_send_char(song_number);	//DAT 2
	serial3_send_char(0xEF);		//footer

} //play()


void Mp3Player::play(uint32_t phone_number) {

	serial3_send_char(0x7E);		//header
	serial3_send_char(0xFF);		//header
	serial3_send_char(0x06);		//header
	serial3_send_char(0x03);		//CMD
	serial3_send_char(0x00);		//FEEDBACK
	serial3_send_char(0x00);		//DAT 1
	serial3_send_char(phone_number);	//DAT 2
	serial3_send_char(0xEF);		//footer

} //play()


void Mp3Player::play_in_loop(uint8_t song_number) {

	serial3_send_char(0x7E);		//header
	serial3_send_char(0xFF);		//header
	serial3_send_char(0x06);		//header
	serial3_send_char(0x08);		//CMD
	serial3_send_char(0x00);		//FEEDBACK
	serial3_send_char(0x00);		//DAT 1
	serial3_send_char(song_number);	//DAT 2
	serial3_send_char(0xEF);		//footer

} //play_in_loop()


void Mp3Player::stop(void) {

		serial3_send_char(0x7E);		//header
		serial3_send_char(0xFF);		//header
		serial3_send_char(0x06);		//header
		serial3_send_char(0x16);		//CMD
		serial3_send_char(0x00);		//FEEDBACK
		serial3_send_char(0x00);		//DAT 1
		serial3_send_char(0x00);		//DAT 2
		serial3_send_char(0xEF);		//footer

} //stop()


void Mp3Player::reset(void) {

	serial3_send_char(0x7E);		//header
	serial3_send_char(0xFF);		//header
	serial3_send_char(0x06);		//header
	serial3_send_char(0x0C);		//CMD
	serial3_send_char(0x00);		//FEEDBACK
	serial3_send_char(0x00);		//DAT 1
	serial3_send_char(0x00);		//DAT 2
	serial3_send_char(0xEF);		//footer

	delay(1000);

} //reset()


void Mp3Player::send_status_request(void) {
	for (uint8_t i = 0; i < RX_FRAME_SIZE; i++) {
		rx_frame[i] = 0;
	}

	serial3_send_char(0x7E);		//header
	serial3_send_char(0xFF);		//header
	serial3_send_char(0x06);		//header
	serial3_send_char(0x42);		//CMD (0x42 == ask for mp3 status)
	serial3_send_char(0x01);		//FEEDBACK (0x01 == request feedback)
	serial3_send_char(0x00);		//DAT 1
	serial3_send_char(0x00);		//DAT 2
	serial3_send_char(0xEF);		//footer
} //send_status_request()
