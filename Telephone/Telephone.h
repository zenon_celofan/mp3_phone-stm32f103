#ifndef TELEPHONE_H_
#define TELEPHONE_H_

#include "stm32f10x_gpio.h"


#define DEBOUNCE_THRESHOLD 				5


class Telephone {

private:

public:
	GPIO_TypeDef * 	pulse_gpio;
	uint16_t		pulse_pin;

	GPIO_TypeDef * 	dial_gpio;
	uint16_t		dial_pin;

	GPIO_TypeDef * 	handset_gpio;
	uint16_t		handset_pin;


	Telephone(void);
	bool is_handset_picked_up(void);
	bool is_dialing(void);
	bool is_number_valid(uint32_t);
	void dial(uint32_t, uint32_t *, uint8_t);
};



#endif /* TELEPHONE_H_ */
